import axios from 'axios';

export const getUser = (id) => {
    return axios.get(`https://jsonplaceholder.typicode.com/users/${id}`);
};