import React, { useState, useEffect, useCallback } from 'react';
import { getPost } from '../../api/post';
import { getUser } from '../../api/user';
import Card from '../card';
import Loader from '../loader';
import ErrorMessage from '../error-message';

function PostContainer({ onBackClick, id }) {
    const [post, setPost] = useState({});
    const [user, setUser] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [isFailed, setIsFailed] = useState(false);

    const getData = useCallback(async () => {
        try {
            const { data: postData } = await getPost(id);
            setPost(postData);
            const { data: userData } = await getUser(postData.userId);
            setUser(userData);
            setIsLoading(false);
        } catch (e) {
            setIsFailed(true);
        }
    }, [id, setPost, setUser, setIsLoading, setIsFailed]);

    useEffect(() => {
        getData();
    }, [getData]);

    if (isFailed) return <ErrorMessage />;
    if (isLoading) return <Loader />;

    return (
        <Card
            userName={user.username}
            userRealName={user.name}
            title={post.title}
            body={post.body}
            onBackClick={onBackClick}
        />
    );
}

export default PostContainer;