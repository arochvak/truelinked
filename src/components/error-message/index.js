import React from 'react';
import Typography from '@material-ui/core/Typography';

function ErrorMessage({ message = "Something wrong... Try later please" }) {
    return <Typography variant="h2">{message}</Typography>
}

export default ErrorMessage;