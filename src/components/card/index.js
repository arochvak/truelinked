import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    title: {
        textAlign: 'center',
        marginBottom: theme.spacing(1)
    },
    userInfo: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
        marginBottom: theme.spacing(1)
    },
    backLink: {
        marginBottom: theme.spacing(3)
    },
    backIcon: {
        fontSize: theme.typography.fontSize
    },
}));

function PostCard({ userName, userRealName, title, body, onBackClick }) {
    const classes = useStyles();
    const onBackLinkClick = (e) => {
        e.preventDefault();
        onBackClick();
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Typography variant="h1" className={classes.title}>{title}</Typography>
            </Grid>
            <Grid item xs={12}>
                <Typography className={classes.userInfo}>{userName} ({userRealName})</Typography>
            </Grid>
            {onBackClick && <Grid item xs={12} className={classes.backLink}>
                <Link href="#" onClick={onBackLinkClick}>
                    <ArrowBackIcon className={classes.backIcon} /> Go back
                </Link>
            </Grid>}
            <Grid item xs={12}>
                <Typography>{body}</Typography>
            </Grid>
        </Grid>
    )
}

export default PostCard;