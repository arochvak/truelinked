import React, { useState, useEffect, useCallback } from 'react';
import { getPosts } from '../../api/posts';
import Table from '../table';
import Loader from '../loader';
import ErrorMessage from '../error-message';

function PostsContainer({ onRowClick }) {
    const headers = [{ name: 'Id', value: 'id' }, { name: 'userId', value: 'userId' }, { name: 'title', value: 'title' }];

    const [posts, setPosts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isFailed, setIsFailed] = useState(false);

    const getAllPosts = useCallback(async () => {
        try {
            const { data } = await getPosts();
            setPosts(data);
            setIsLoading(false);
        } catch (e) {
            setIsFailed(true);
        }
    }, [setPosts, setIsLoading, setIsFailed]);

    useEffect(() => {
        getAllPosts();
    }, [getAllPosts]);

    if (isFailed) return <ErrorMessage />;
    if (isLoading) return <Loader />;

    return (
        <Table
            data={posts}
            headers={headers}
            onRowClick={onRowClick}
        />
    );
}

export default PostsContainer;