import React from 'react';
import {
  useHistory
} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PostsContainer from '../../components/posts-container';

function App() {
  const history = useHistory();

  const onRowClick = (e, rowItem) => {
    history.push(`post/${rowItem.id}`);
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h2">Posts</Typography>
      </Grid>
      <Grid item xs={12}>
        <PostsContainer onRowClick={onRowClick} />
      </Grid>
    </Grid>
  );
}

export default App;
