import React from 'react';
import {
    useHistory,
    useParams
} from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import PostContainer from '../../components/post-container';

function App() {
    const { id } = useParams();
    const history = useHistory();

    const onBackClick = () => {
        history.push('/');
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Container>
                    <PostContainer onBackClick={onBackClick} id={id} />
                </Container>
            </Grid>
        </Grid>
    );
}

export default App;
