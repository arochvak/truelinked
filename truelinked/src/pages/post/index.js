import React from 'react';
import {
    Link,
    useParams
} from "react-router-dom";

function App() {
    const { id } = useParams();
    return (
        <div>
            <div>Post {id}</div>
            <Link to="/posts">Posts</Link>
        </div>
    );
}

export default App;
